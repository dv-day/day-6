function loadAllStudents(){
    var responseData = null;
    var data = fetch('https://dv-student-backend-2019.appspot.com/students')
    .then((response) => {
        console.log(response)
        return response.json()
    }).then((json) => {
        responseData= json 
        var resultElement = document.getElementById('result')
        resultElement.innerHTML = JSON.stringify(responseData, null, 2)
   })
}

async function loadAllStudentsAsync() {
    let response = await fetch('https://dv-student-backend-2019.appspot.com/students')
    let data = await response.json()
    var resultElement = document.getElementById('result')
    // resultElement.innerHTML = JSON.stringify(data, null, 2)
    return data
}

function createResultTable(data){
    let resultElement = document.getElementById('resultTable')
    let tableNode =  document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    var tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'studentID'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'GPA'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        for(let i = 0; i<json.length; i++){
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

            var dataFirstCol = document.createElement('th')
            dataFirstCol.setAttribute('scope','row')
            dataFirstCol.innerText = currentData['id']
            dataRow.appendChild(dataFirstCol)

            var colNode = null;
            colNode = document.createElement('td')
            colNode.innerText = currentData['studentId']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            colNode.innerText = currentData['name']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            colNode.innerText = currentData['surname']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            colNode.innerText = currentData['gpa']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            var imgNode = document.createElement('img')
            imgNode.setAttribute('src', currentData['image'])
            imgNode.style.width = '50px'
            imgNode.style.height = '50px'
            dataRow.appendChild(imgNode)

        }
    })
}

async function loadOneStudents(){
    let studentId = document.getElementById('queryId').value
    if(studentId != '' && studentId != null){
        let response = await fetch('https://dv-student-backend-2019.appspot.com/students/'+ studentId) 
        let data = await response.json()
        return data   
    }
}

function createOneTable(data){
    let resultElement = document.getElementById('resultTable')
    let tableNode =  document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    var tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'studentID'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'GPA'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((obj) => {
        console.log(obj)

            var currentData = obj
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

            var dataFirstCol = document.createElement('th')
            dataFirstCol.setAttribute('scope','row')
            dataFirstCol.innerText = currentData['id']
            dataRow.appendChild(dataFirstCol)

            var colNode = null;
            colNode = document.createElement('td')
            colNode.innerText = currentData['studentId']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            colNode.innerText = currentData['name']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            colNode.innerText = currentData['surname']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            colNode.innerText = currentData['gpa']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            var imgNode = document.createElement('img')
            imgNode.setAttribute('src', currentData['image'])
            imgNode.style.width = '50px'
            imgNode.style.height = '50px'
            dataRow.appendChild(imgNode)
    })
}
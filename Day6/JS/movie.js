async function loadAllMoviesAsync() {
    let response = await fetch('https://dv-excercise-backend.appspot.com/movies')
    let data = await response.json()
    // var resultElement = document.getElementById('result')
    // resultElement.innerHTML = JSON.stringify(data, null, 2)
    return data

}

function createResultTable(data){
    let resultElement = document.getElementById('resultTable')
    let tableNode =  document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table table-hover table-info')

    var tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.setAttribute('class', 'bg-danger')
    tableHeaderNode.innerHTML = 'Name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.setAttribute('class', 'bg-warning')

    tableHeaderNode.innerHTML = 'Synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        for(let i = 0; i<json.length; i++){
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

            var colNode = null;
            colNode = document.createElement('th')
            colNode.setAttribute('class', 'bg-danger')
            colNode.innerText = currentData['name']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            colNode.setAttribute('class', 'bg-warning')
            colNode.innerText = currentData['synopsis']
            dataRow.appendChild(colNode)

            colNode = document.createElement('td')
            var imgNode = document.createElement('img')
            imgNode.setAttribute('src', currentData['imageUrl'])
            // imgNode.style.border='25px solid green'
            imgNode.style.width = '150px'
            imgNode.style.height = '150px'
            dataRow.appendChild(imgNode)

        }
    })
}

async function loadOneMovie(){
    let name = document.getElementById('queryName').value
    if(name != '' && name != null){
        let response = await fetch('https://dv-excercise-backend.appspot.com/movies/'+ name) 
        let data = await response.json()
        return data   
    }
}

function createOneTable(data){
    let text =  document.createElement('h2')
    text.innerHTML = "Search Results"

    let resultElement = document.getElementById('resultTable')
    let tableNode =  document.createElement('table')

    resultElement.appendChild(text)

    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table table-striped table-dark')

    var tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRowNode = document.createElement('tr')           
    tableRowNode.setAttribute('class', 'bg-primary')
    tableHeadNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerHTML = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((obj) => {
        console.log(obj)
        for(let i = 0; i<obj.length; i++){

        var currentData = obj[i]
        var dataRow = document.createElement('tr')
        tableNode.appendChild(dataRow)

        var colNode = null;
        colNode = document.createElement('td')
        colNode.innerText = currentData['name']
        dataRow.appendChild(colNode)

        colNode = document.createElement('td')
        colNode.innerText = currentData['synopsis']
        dataRow.appendChild(colNode)

        colNode = document.createElement('td')
        var imgNode = document.createElement('img')
        imgNode.setAttribute('src', currentData['imageUrl'])
        imgNode.style.margin = '10px'
        imgNode.style.width = '120px'
        imgNode.style.height = '120px'
        dataRow.appendChild(imgNode)
        }
    })
}

